package ru.habibrahmanov.tm.enumeration;

public enum Status {
    PLANNED("PLANNED"),
    INPROGRESS("INPROGRESS"),
    READY("READY");

    private String status;

    public String getStatus() {
        return status;
    }

    Status(String status) {
        this.status = status;
    }

    public String displayName() {
        return status;
    }


}
