package ru.habibrahmanov.tm.rest;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.dto.TaskDtos;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Getter
@Setter
@Named
@SessionScoped
public class TaskRest {

    @Nullable private String id;
    @Nullable private String projectId;
    @Nullable private String name;
    @Nullable private String description;
    @Nullable private String status;
    @Nullable private String dateBegin;
    @Nullable private String dateEnd;

    @NotNull
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @NotNull final static String URL = "http://localhost:8080/";

    private final Logger LOG = LoggerFactory.getLogger(TaskRest.class);

    public RedirectView create(@Nullable final String projectId, @Nullable final String name, @Nullable final String description,
                               @Nullable final String dateBegin, @Nullable final String dateEnd) throws ParseException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setProjectId(projectId);
        taskDto.setName(name);
        taskDto.setDescription(description);
        taskDto.setDateBegin(dateFormat.parse(dateBegin));
        taskDto.setDateEnd(dateFormat.parse(dateEnd));
        restTemplate.postForObject(URL + "task-create", taskDto, TaskDto.class);
        return new RedirectView("tasks/" + projectId + "?faces-redirect=true");
    }

    public RedirectView update(@Nullable final String id, @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
                               @Nullable final Status status, @Nullable final String dateBegin, @Nullable final String dateEnd) throws ParseException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(id);
        taskDto.setProjectId(projectId);
        taskDto.setName(name);
        taskDto.setDescription(description);
        taskDto.setStatus(status);
        taskDto.setDateBegin(dateFormat.parse(dateBegin));
        taskDto.setDateEnd(dateFormat.parse(dateEnd));
        restTemplate.put(URL + "task-update", taskDto, TaskDto.class);
        return new RedirectView("/tasks/" + projectId + "?faces-redirect=true");
    }

    public List<TaskDto> findAll(String projectId){
        RestTemplate restTemplate = new RestTemplate();
        TaskDtos taskDtos = restTemplate.getForObject(URL + "tasks/" + projectId, TaskDtos.class);
        return taskDtos.getTaskDtos();
    }

    public RedirectView removeOne(@NotNull final String projectId, @NotNull final String id){
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "task-delete/"+ projectId + "/" + id);
        return new RedirectView("tasks/" + projectId);
    }
}
