package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    void persist(@NotNull Task task) throws ParseException;

    void insert(
            @NotNull Project project, @NotNull String name,
            @NotNull String description, @NotNull String dateBegin, @NotNull String dateEnd
    ) throws ParseException;

    @Nullable Task findOne(@NotNull String id);

    @Nullable Task findOne(@Nullable String projectId, @Nullable String id);

    @NotNull
    List<Task> findAll(@NotNull String projectId);

    @Transactional
    void update(@NotNull Task task
    ) throws ParseException;

    @Transactional
    void update(@NotNull String projectId, @NotNull String id, @NotNull String name,
                @NotNull String description, @NotNull Status status, @NotNull String dateBegin, @NotNull String dateEnd
    ) throws ParseException;

    @NotNull
    List<Task> searchByString(@NotNull String taskId, @NotNull String string);

    @NotNull List<Task> findAll();

    @Transactional
    void removeOne(@Nullable String projectId, @Nullable String taskId);

    void removeAll(@NotNull String projectId);
}
