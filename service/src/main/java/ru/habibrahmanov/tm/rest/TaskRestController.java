package ru.habibrahmanov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.dto.TaskDtos;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskRestController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping("/tasks/{projectId}")
    public TaskDtos findAll(@NotNull @PathVariable(name = "projectId") final String projectId) {
        @Nullable final List<Task> taskList = taskService.findAll(projectId);
        @Nullable final List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertEntityToDto(task));
        }
        @Nullable final TaskDtos taskDtos = new TaskDtos();
        taskDtos.setTaskDtos(taskDtoList);
        return taskDtos;
    }

    @Nullable
    @GetMapping("/tasks/{projectId}/{taskId}")
    public TaskDto findOne(@NotNull @PathVariable(name = "projectId") final String projectId,
                           @NotNull @PathVariable(name = "taskId") final String taskId) {
        return convertEntityToDto(taskService.findOne(projectId, taskId));
    }

    @DeleteMapping("/task-delete/{projectId}/{id}")
    public RedirectView delete(@Nullable @PathVariable(name = "projectId") final String projectId, @Nullable @PathVariable(name = "id") final String id) {
        taskService.removeOne(projectId, id);
        return new RedirectView("/tasks" + projectId);
    }

    @GetMapping("/task-create")
    public RedirectView create(
            @Nullable @RequestParam(name = "name") final String name, @Nullable @RequestParam(name = "description") final String description,
            @Nullable @RequestParam(name = "dateBegin") final String dateBegin, @RequestParam(name = "dateEnd") @Nullable final String dateEnd,
            @Nullable @RequestParam(name = "projectId") final String projectId
    ) throws ParseException {
        taskService.insert(projectService.findOne(projectId), name, description, dateBegin, dateEnd);
        return new RedirectView("/tasks/" + projectId);
    }

    @PostMapping("/task-create")
    public RedirectView create(@Nullable @RequestBody final TaskDto taskDto) throws ParseException {
        taskService.persist(convertDtoToEntity(taskDto));
        return new RedirectView("/tasks/" + taskDto.getProjectId());
    }

    @GetMapping("/task-update")
    public RedirectView update(
            @NotNull @RequestParam(name = "projectId") final String projectId, @NotNull @RequestParam(name = "id") final String id, @NotNull @RequestParam(name = "name") final String name,
            @Nullable @RequestParam(name = "description", required = false) final String description,
            @Nullable @RequestParam(name = "status") final Status status, @Nullable @RequestParam(name = "dateBegin") final String dateBegin, @RequestParam(name = "dateEnd") @Nullable final String dateEnd
    ) throws ParseException {
        taskService.update(projectId, id, name, description, status, dateBegin, dateEnd);
        return new RedirectView("/tasks" + projectId);
    }

    @PutMapping("/task-update")
    public RedirectView update(@Nullable @RequestBody final TaskDto taskDto) throws ParseException {
        taskService.update(convertDtoToEntity(taskDto));
        return new RedirectView("/tasks/" + taskDto.getProjectId());
    }

    @Nullable
    public TaskDto convertEntityToDto(@Nullable final Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setStatus(task.getStatus());
        taskDto.setDateBegin(task.getDateBegin());
        taskDto.setDateEnd(task.getDateEnd());
        return taskDto;
    }

    @Nullable
    public Task convertDtoToEntity(@Nullable final TaskDto taskDto) {
        Task task = new Task();
        task.setProject(projectService.findOne(taskDto.getProjectId()));
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setStatus(taskDto.getStatus());
        task.setDateBegin(taskDto.getDateBegin());
        task.setDateEnd(taskDto.getDateEnd());
        return task;
    }
}
