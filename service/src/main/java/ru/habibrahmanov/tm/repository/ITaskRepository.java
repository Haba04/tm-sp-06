package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    List<Task> findByProjectId(@NotNull @Param("projectId") String projectId);

    @Modifying
    @Query("DELETE FROM Task WHERE project.id = :projectId AND id = :id")
    void deleteByProjectIdAndId(@NotNull @Param("projectId") String projectId, @NotNull @Param("id") String id);

    Task findByProjectIdAndId(@NotNull String projectId, @NotNull String id);

    @Query("SELECT task FROM Task task WHERE task.id = :taskId AND task.name LIKE :string OR task.name LIKE :string")
    List<Task> searchByString(@NotNull @Param("taskId") String taskId, @NotNull @Param("string") String string);

    @Modifying
    @Query("DELETE FROM Task WHERE project.id = :projectId")
    void deleteAllByProjectId(@NotNull @Param("projectId") String projectId);

    @Modifying
    @Query("UPDATE Task SET name = :name, description = :description, dateBegin = :dateBegin, dateEnd = :dateEnd, status = :status WHERE id = :id AND project.id = :projectId")
    void update(@Nullable @Param("projectId") String projectId, @Nullable @Param("id") String id, @Nullable @Param("name") String name, @Nullable @Param("description") String description,
                @Nullable @Param("status") Status status, @Nullable @Param("dateBegin") Date dateBegin, @Nullable @Param("dateEnd") Date dateEnd);
}
