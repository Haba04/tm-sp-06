package ru.habibrahmanov.tm.managedbean;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.repository.IProjectRepository;
import ru.habibrahmanov.tm.repository.ITaskRepository;

import java.text.SimpleDateFormat;

public abstract class AbstractBean {
    @NotNull
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    protected IProjectRepository projectRepository;
    @Autowired
    protected IProjectService projectService;
    @Autowired
    protected ITaskRepository taskRepository;
    @Autowired
    protected ITaskService taskService;
}
