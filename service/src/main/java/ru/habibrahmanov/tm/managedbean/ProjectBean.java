package ru.habibrahmanov.tm.managedbean;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.util.List;

@Getter
@Setter
@Named("projectBean")
@ViewScoped
public class ProjectBean extends AbstractBean {

    @Nullable private String projectId;
    @Nullable private String name;
    @Nullable private String description;
    @Nullable private String dateBegin;
    @Nullable private String dateEnd;
    @Nullable private String status;
    @Nullable private String string;
    
    public String create(@Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
                         @Nullable final String dateEnd) throws ParseException {
        projectService.insert(name, description, dateBegin, dateEnd);
        return "/project?faces-redirect=true";
    }

    @Nullable
    public Project findOne(@Nullable final String projectId) {
        return projectService.findOne(projectId);
    }

    @NotNull
    public List<Project> getFindAll() {
        return projectService.findAll();
    }

    public void removeAll() {
        projectService.removeAll();
    }

    public String removeOne(@Nullable final String projectId) {
        projectService.removeOne(projectId);
        return "/project?faces-redirect=true";
    }

    public String update(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final Status status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws ParseException {
        projectService.update(projectId, name, description, status, dateBegin, dateEnd);
        return "/project?faces-redirect=true";
    }

    @Nullable
    public List<Project> searchByString() {
        return projectService.searchByString(projectId, string);
    }
}