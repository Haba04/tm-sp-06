package ru.habibrahmanov.tm.managedbean;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.util.List;

@Getter
@Setter
@Named
@ViewScoped
public class TaskBean extends AbstractBean {

    @Nullable private String projectId;
    @Nullable private String taskId;
    @Nullable private String name;
    @Nullable private String description;
    @Nullable private String dateBegin;
    @Nullable private String dateEnd;
    @Nullable private String status;
    @Nullable private String string;

    public String insert(
            @NotNull final Project project, @NotNull final String name,
            @NotNull final String description, @NotNull final String dateBegin, @NotNull final String dateEnd
    ) throws ParseException {
        taskService.insert(project, name, description, dateBegin, dateEnd);
        return "task?faces-redirect=true";
    }

    @Nullable
    public Task findOne(@Nullable final String id) {
        return taskService.findOne(id);
    }

    @NotNull
    public List<Task> findAll(@Nullable final String projectId) {
        return taskService.findAll(projectId);
    }

    @NotNull
    public List<Task> findAll() {
        return taskService.findAll();
    }

    public String removeOne(@Nullable final String projectId, @Nullable final String id) {
        taskService.removeOne(projectId, id);
        return "task?faces-redirect=true";
    }

    public void removeAll(@Nullable final String projectId) {
        taskService.removeAll(projectId);
    }

    public String update(@NotNull final String projectId, @NotNull final String id, @NotNull final String name, @NotNull final String description,
                       @NotNull final Status status, @NotNull final String dateBegin, @NotNull final String dateEnd
    ) throws ParseException {
        taskService.update(projectId, id, name, description, status, dateBegin, dateEnd);
        return "task?faces-redirect=true";
    }

    public List<Task> searchByString(@NotNull final String taskId, @NotNull final String string) {
        return taskService.searchByString(taskId, string);
    }
}
