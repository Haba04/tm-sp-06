package ru.habibrahmanov.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@JacksonXmlRootElement(localName = "task")
public class TaskDto {

    private String id = UUID.randomUUID().toString();
    private String projectId;
    private String name;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateBegin;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateEnd;
    private Status status = Status.PLANNED;

    public TaskDto(@NotNull final String projectId, @NotNull final String name, @NotNull final String description,
                   @NotNull final Date dateBegin, @NotNull final Date dateEnd) {
        this.projectId = projectId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }
}