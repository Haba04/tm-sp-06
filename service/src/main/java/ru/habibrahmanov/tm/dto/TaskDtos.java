package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TaskDtos {
    private List<TaskDto> taskDtos;
}
