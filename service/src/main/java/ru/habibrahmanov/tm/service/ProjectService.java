package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.repository.IProjectRepository;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd
    ) throws ParseException {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (dateBegin == null || dateBegin.isEmpty()) return;
        if (dateEnd == null || dateEnd.isEmpty()) return;
        projectRepository.save(new Project(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    @Override
    public Project persist(@Nullable final Project project) {
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findById(projectId).orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Transactional
    @Override
    public void removeOne(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.deleteById(projectId);
    }

    @Transactional
    @Override
    public void update(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final Status status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws ParseException {
        if (projectId == null || projectId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (status == null) return;
        if (dateBegin == null || dateBegin.isEmpty()) return;
        if (dateEnd == null || dateEnd.isEmpty()) return;
        projectRepository.update(projectId, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Transactional
    @Override
    public void update(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final Status status, @Nullable final Date dateBegin, @Nullable final Date dateEnd
    ) throws ParseException {
        if (projectId == null || projectId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (status == null) return;
        if (dateBegin == null) return;
        if (dateEnd == null) return;
        projectRepository.update(projectId, name, description, status, dateBegin, dateEnd);
    }

    @Transactional
    @Override
    public void update(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws ParseException {
        if (projectId == null || projectId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (status == null) return;
        if (dateBegin == null || dateBegin.isEmpty()) return;
        if (dateEnd == null || dateEnd.isEmpty()) return;
//        switch (status.toLowerCase()){
//            case "planned" :
//                projectRepository.update(projectId, name, description, Status.PLANNED, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
//            case "inprogress" :
//                projectRepository.update(projectId, name, description, Status.INPROGRESS, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
//            case "ready" :
//                projectRepository.update(projectId, name, description, Status.READY, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
//        }
        projectRepository.update(projectId, name, description, Status.valueOf(status.toUpperCase()), dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Nullable
    @Override
    public List<Project> searchByString(@NotNull final String projectId, @NotNull final String string) {
        @Nullable final List<Project> projectList = projectRepository.getProjectsByString(projectId, string);
        return projectList;
    }
}